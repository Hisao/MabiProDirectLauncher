﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace MabiProDirectLauncher
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
#if !DEBUG
            MessageBox.Show("This program is for advanced users\nwho need to customize their client IPs\nbefore launching MabiPro.\n\nNo support will be given due to misuse\nof this program.\n\nThis is your only warning.\n\n- MabiPro Staff", "Caution", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
#endif
            Application.Run(new Settings());
        }
    }
}
