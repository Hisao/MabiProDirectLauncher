﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace MabiProDirectLauncher
{
    public partial class Settings : Form
    {
        public Settings()
        {
            InitializeComponent();
        }

        private void launchMabiPro_Click(object sender, EventArgs e)
        {
            string clientArgs = "code:1622 ver:200 logip:" + loginServer.Text
                + " logport:" + loginPort.Value + " chatip:" + chatServer.Text + " chatport:"
                + chatPort.Value + " setting:\"file://data/features.xml=Regular, Japan\" jpG13";
#if DEBUG
            MessageBox.Show(clientArgs, "Test", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
#else
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.FileName = "Client.exe";
            startInfo.Arguments = clientArgs;
            Process.Start(startInfo);
            Application.Exit();
#endif
        }
    }
}
