﻿namespace MabiProDirectLauncher
{
    partial class Settings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.loginServer = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.loginPort = new System.Windows.Forms.NumericUpDown();
            this.launchMabiPro = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.chatServer = new System.Windows.Forms.ComboBox();
            this.chatPort = new System.Windows.Forms.NumericUpDown();
            ((System.ComponentModel.ISupportInitialize)(this.loginPort)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chatPort)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Login Server";
            // 
            // loginServer
            // 
            this.loginServer.DisplayMember = "vier.mabi.pro";
            this.loginServer.FormattingEnabled = true;
            this.loginServer.Items.AddRange(new object[] {
            "drei.mabi.pro",
            "vier.mabi.pro",
            "funf.mabi.pro"});
            this.loginServer.Location = new System.Drawing.Point(16, 30);
            this.loginServer.Name = "loginServer";
            this.loginServer.Size = new System.Drawing.Size(97, 21);
            this.loginServer.TabIndex = 1;
            this.loginServer.Text = "vier.mabi.pro";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Login Port";
            // 
            // loginPort
            // 
            this.loginPort.Location = new System.Drawing.Point(16, 75);
            this.loginPort.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.loginPort.Name = "loginPort";
            this.loginPort.Size = new System.Drawing.Size(97, 20);
            this.loginPort.TabIndex = 3;
            this.loginPort.Value = new decimal(new int[] {
            11000,
            0,
            0,
            0});
            // 
            // launchMabiPro
            // 
            this.launchMabiPro.Location = new System.Drawing.Point(13, 114);
            this.launchMabiPro.Name = "launchMabiPro";
            this.launchMabiPro.Size = new System.Drawing.Size(341, 36);
            this.launchMabiPro.TabIndex = 4;
            this.launchMabiPro.Text = "Launch MabiPro";
            this.launchMabiPro.UseVisualStyleBackColor = true;
            this.launchMabiPro.Click += new System.EventHandler(this.launchMabiPro_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(145, 13);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(63, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Chat Server";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(148, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Chat Port";
            // 
            // chatServer
            // 
            this.chatServer.FormattingEnabled = true;
            this.chatServer.Items.AddRange(new object[] {
            "vier.mabi.pro"});
            this.chatServer.Location = new System.Drawing.Point(148, 30);
            this.chatServer.Name = "chatServer";
            this.chatServer.Size = new System.Drawing.Size(97, 21);
            this.chatServer.TabIndex = 7;
            this.chatServer.Text = "vier.mabi.pro";
            // 
            // chatPort
            // 
            this.chatPort.Location = new System.Drawing.Point(148, 75);
            this.chatPort.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.chatPort.Name = "chatPort";
            this.chatPort.Size = new System.Drawing.Size(97, 20);
            this.chatPort.TabIndex = 8;
            this.chatPort.Value = new decimal(new int[] {
            8002,
            0,
            0,
            0});
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(366, 162);
            this.Controls.Add(this.chatPort);
            this.Controls.Add(this.chatServer);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.launchMabiPro);
            this.Controls.Add(this.loginPort);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.loginServer);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Launcher Settings";
            ((System.ComponentModel.ISupportInitialize)(this.loginPort)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chatPort)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox loginServer;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.NumericUpDown loginPort;
        private System.Windows.Forms.Button launchMabiPro;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox chatServer;
        private System.Windows.Forms.NumericUpDown chatPort;
    }
}

